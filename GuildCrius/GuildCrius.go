package GuildCrius

import "context"

// For the context value with name permissions.
type Permissions interface {
	IsBotOwner(string) bool
	IsServerOwner(string, string) (bool, error)
	IsServerAdmin(string, string) (bool, error)
	IsServerMod(string, string) (bool, error)
}

func GetPermissions(ctx context.Context) Permissions {
	return ctx.Value("permissions").(Permissions)
}
