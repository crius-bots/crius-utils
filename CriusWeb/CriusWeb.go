package CriusWeb

import (
	"context"
	"fmt"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-redis/redis/v8"
	"github.com/gorilla/csrf"
	"github.com/rbcervilla/redisstore/v8"
	cc "gitlab.com/RPGPN/criuscommander/v2"
	"html/template"
	"io/fs"
	"net/http"
)

func GetWebController(ctx context.Context) *WebController {
	return ctx.Value("webController").(*WebController)
}

var DefaultMiddlewareStack = chi.Middlewares{middleware.Recoverer, middleware.RequestID, middleware.Logger}

type Route struct {
	IsProtected bool
	Path        string
	Handlers    map[string]http.HandlerFunc // a map of HTTP methods to handler funcs. specify only the ones you need
	Handler     http.Handler                // sometimes you just need to pass a handler. pass this **instead** of the Handlers map.
}

type Config struct {
	AuthMiddleware func(store *redisstore.RedisStore) func(http.Handler) http.Handler

	// Middlewares
	Middlewares chi.Middlewares

	// If you need to control what the data passed in to the template looks like, i.e. putting a username in the navbar
	BuildTemplateData func(r *http.Request, title string, data interface{}) interface{}

	TemplateFuncMap template.FuncMap

	StaticFiles fs.FS
	Partials    fs.FS
	Layout      string

	// Session Redis store
	StoreClient *redis.Client

	// /app
	AppRouteFn func(*WebController) http.HandlerFunc

	// /app/plugins
	PluginsRouteFn func(*WebController) http.HandlerFunc

	CSRFKey []byte
}

func NewWebController(config *Config) (*WebController, error) {
	// set up the session store
	store, err := redisstore.NewRedisStore(context.Background(), config.StoreClient)
	if err != nil {
		return nil, err
	}

	router := chi.NewRouter()

	if config.Middlewares != nil {
		router.Use(config.Middlewares...)
	} else {
		router.Use(DefaultMiddlewareStack...)
	}

	router.Use(middleware.WithValue("store", store))

	// set up the static file server
	router.Handle("/static/*", http.FileServer(http.FS(config.StaticFiles)))

	csrfMw := csrf.Protect(config.CSRFKey, csrf.Path("/app"), csrf.HttpOnly(true))

	wc := &WebController{
		router:              router,
		authenticatedRouter: router.With(config.AuthMiddleware(store), csrfMw),
		config:              config,
		plugins:             map[string]*cc.PluginJSON{},
	}

	if config.AppRouteFn != nil {
		wc.authenticatedRouter.HandleFunc("/app", config.AppRouteFn(wc))
	}

	if config.PluginsRouteFn != nil {
		wc.authenticatedRouter.HandleFunc("/app/plugins", config.PluginsRouteFn(wc))
	}

	return wc, nil
}

type WebController struct {
	router              chi.Router
	authenticatedRouter chi.Router

	config  *Config
	plugins map[string]*cc.PluginJSON
}

func (wc *WebController) GetRoutesList() []chi.Route {
	return wc.router.Routes()
}

// RegisterRoute does not support registering authenticated routes
func (wc *WebController) RegisterRoute(pluginName string, route *Route) {
	if route.IsProtected {
		return
	} else {
		if route.Handlers == nil && route.Handler != nil {
			wc.router.Handle(fmt.Sprintf("/%s%s", pluginName, route.Path), route.Handler)
			return
		}

		if route.Handlers != nil {
			for method, h := range route.Handlers {
				wc.router.MethodFunc(method, fmt.Sprintf("/%s%s", pluginName, route.Path), h)
			}
		}
	}
}

func (wc *WebController) RegisterRoutes(pluginName string, routes []*Route) {
	subRouter := chi.NewRouter()

	for _, route := range routes {
		if route.IsProtected {
			// protected routes get mounted to the subrouter to stop us from calling Mount twice which is a panic
			if route.Handlers == nil && route.Handler != nil {
				subRouter.Handle(route.Path, route.Handler)
				continue
			}

			if route.Handlers != nil {
				for method, h := range route.Handlers {
					subRouter.MethodFunc(method, route.Path, h)
				}
			}
		} else {
			wc.RegisterRoute(pluginName, route)
		}
	}

	wc.authenticatedRouter.Mount("/app/plugins/"+pluginName, subRouter)
}

func (wc *WebController) GetServer(addr string) *http.Server {
	return &http.Server{
		Addr:    addr,
		Handler: wc.router,
	}
}

func (wc *WebController) PrepareTemplate(html string) (*template.Template, error) {
	tmpl := template.New("").Funcs(wc.config.TemplateFuncMap)

	tmpl, err := tmpl.ParseFS(wc.config.Partials, "*.gohtml")
	if err != nil {
		return nil, err
	}

	tmpl, err = tmpl.Parse(wc.config.Layout)
	if err != nil {
		return nil, err
	}

	tmpl, err = tmpl.Parse(html)
	if err != nil {
		return nil, err
	}

	return tmpl, nil
}

func (wc *WebController) Render(w http.ResponseWriter, r *http.Request, t *template.Template, title string, data interface{}) error {
	shapedData := wc.config.BuildTemplateData(r, title, data)

	return t.Execute(w, shapedData)
}

func (wc *WebController) DoError(w http.ResponseWriter, r *http.Request, error string, code int) {
	if r.Header.Get("HX-Request") != "" {
		// it's a htmx request, send code 200 and the error
		w.Write([]byte(error))
		return
	}

	// normal http request, treat it normally (read: with a non-200 code)
	http.Error(w, error, code)
}

// call this function after you call RegisterRoutes to allow your plugin to show in the list at /app/plugins
func (wc *WebController) RegisterPlugin(path string, info *cc.PluginJSON) {
	wc.plugins[path] = info
}

func (wc *WebController) GetPluginList() map[string]*cc.PluginJSON {
	return wc.plugins
}

func (wc *WebController) ReadAndPrepTemplate(fsys fs.FS, path string) (*template.Template, error) {
	data, err := fs.ReadFile(fsys, path)
	if err != nil {
		return nil, err
	}

	return wc.PrepareTemplate(string(data))
}
