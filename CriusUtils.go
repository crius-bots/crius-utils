package CriusUtils

import (
	"context"
	"github.com/asaskevich/EventBus"
	"github.com/jmoiron/sqlx"
	cc "gitlab.com/RPGPN/criuscommander/v2"
)

// base settings type

type Settings interface {
	GetDB() *sqlx.DB
}

// funcs to make getting from ctx nicer

func GetSettings(ctx context.Context) Settings {
	return ctx.Value("settings").(Settings)
}

func GetPlatform(ctx context.Context) cc.PlatformType {
	return ctx.Value("platform").(cc.PlatformType)
}

func GetCommander(ctx context.Context) *cc.Commander {
	return ctx.Value("commander").(*cc.Commander)
}

func GetBus(ctx context.Context) EventBus.Bus {
	return ctx.Value("bus").(EventBus.Bus)
}

func GetOtherConfig(ctx context.Context) map[string]interface{} {
	return ctx.Value("config").(map[string]interface{})
}
